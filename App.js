/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow strict-local
 */

import React, {Component} from 'react';
import {StyleSheet, TouchableOpacity,TextInput,Dimensions, Text, View, Image} from 'react-native';
import firebase from './firebase';
var { height } = Dimensions.get('window');
export default class App extends Component {
  constructor (props) {
    super(props);
    this.state ={
      textValue: '',
      count: 0,
      name:'',
      email:'',
      mobile:'',
      isLoading:false
    };
  }

  changeTextInputNombre = text => {
    this.setState ({name: text});
  };
  changeTextInputEmail = text => {
    this.setState ({email: text});
  };
  changeTextInputMobile = text => {
    this.setState ({mobile: text});
  };
  

  storeUser() {
    console.log('Click');

    if (this.state.name === ''){
      alert('Fill at least your name!');
    }else {
      this.setState({
        isLoading: true,
      });

      const db = firebase.firestore();
      db.settings({experimentalForceLongPolling: true});

      db.collection('users').add({
        name: this.state.name,
        email: this.state.email,
        mobile: this.state.mobile,
      }).then((res)=> {
        this.setState({
          name:'',
          email:'',
          mobile:'',
          isLoading: false,
        });
      })
    }
  }

  render() {
    return (
      <View style={styles.container}>
      <View>
        <Text style={styles.text}> Ingrese su Nombre:</Text>
      </View>
      <TextInput 
        style={styles.textInput}
        onChangeText= {text=> this.changeTextInputNombre(text)}
        placeholder="Name"
        placeholderTextColor="#20b2aa"
        value= {this.state.name}
      />
       <View >
        <Text style={styles.text}> Ingrese su Email:</Text>
      </View>
      <TextInput 
        style={styles.textInput}
        placeholder="Email"
        placeholderTextColor="#20b2aa"
        onChangeText= {text=> this.changeTextInputEmail(text)}
        value= {this.state.email}
      />
      <View >
        <Text style={styles.text}> Ingrese su Mobile:</Text>
      </View>
      <TextInput 
        style={styles.textInput}
        placeholder="Mobile"
        placeholderTextColor="#20b2aa"
        onChangeText= {text=> this.changeTextInputMobile(text)}
        value= {this.state.mobile}
      />
      <TouchableOpacity style={styles.button}  onPress={() => {this.storeUser()}}>
        <Text style={styles.textButon}>ADD USER</Text>
      </TouchableOpacity>
    </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    backgroundColor:'darkblue',
    padding: 100,
    paddingBottom: 250,
    height: height
  },
  text: {
    alignItems: 'center',
    padding: 10,
    fontWeight: 'bold',
    color: '#DDDDDD',
    fontSize: 20,
  },
  image: {
    alignItems: 'center',
    justifyContent: 'center',
    padding: 20,
  },
  button: {
    top: 10,
    alignItems: 'center',
    backgroundColor: '#DDDDDD',
    padding: 10,
    borderRadius : 30,
  },
  textInput: {
    height: 40, 
    borderColor: '#DDDDDD', 
    borderWidth: 1,
    fontSize: 18,
    color: '#DDDDDD',
    borderRadius : 10,
  },
  textButon: {
    alignItems: 'center',
    padding: 5,
    fontWeight: 'bold',
    fontSize: 20,
    
  },
  countContainer: {
    alignItems: 'center',
    padding: 10,
  },
  countText: {
    color: '#FF00FF',
  },
})

